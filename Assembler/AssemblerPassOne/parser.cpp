#include "assemblerHeaders.h"

parser::parser()
{
    reader.readTable();
    dReader.readDirective();
    instructionWithNoSymbol.assign("^\\s*([+]?[a-zA-Z]+)\\s+(=[CX]'.+'|\\S+)");
    instructionWithSymbol.assign("^\\s*([a-zA-Z]{1}\\w+)\\s+([+]?[a-zA-Z]+)\\s+(=[CX]'.+'|\\S+)");
    RSUBWithSymbol.assign("^\\s*([+]?[a-zA-Z]+)\\s+RSUB\\s*");
    directiveWithSymbolAndOperand.assign("^\\s*([a-zA-Z]{1}\\w+)\\s+([a-zA-Z]+)\\s+(\\S{1}.+\\S{1})");
    directiveWithNoSymbolAndOperand.assign("^\\s*([a-zA-Z]+)\\s+(\\S{1}.+\\S{1})");
    directiveWithNoSymbolNorOperand.assign("^\\s*([a-zA-Z]+)");
    commentRegex.assign("^\\s*[\\.]{1}.*");

}

bool parser::matchComment(string line)
{
    if (regex_match(line,commentRegex) || isBlank(line))
    {
        isVld = true;
        isComm = true;
        return true;
    }
    return false;

}

bool parser::matchesOperation(string line)
{
    if(matchRSUB(line))
    {
        isVld = true;
        isComm = false;
        hasOprnds = false;
        isDir = false;
        format = 3;
        return true;

    }

    else if(matchInstructionWithSymbol(line))
    {
        isVld= true;
        isComm = false;
        hasSym = true;
        hasOprnds = true;
        isDir = false;
        return true;
    }
    else if (matchInstructionWithNoSymbol(line))
    {
        isVld = true;
        isComm = false;
        hasSym = false;
        hasOprnds = true;
        isDir = false;
        return true;
    }
    return false;


}

bool parser::matchRSUB(string line)
{
    std::smatch match;
    if (std::regex_search(line, match, regex("^\\s*([a-zA-Z]{1}\\w+)\\s+([\\+]?RSUB)\\s*$",regex_constants::icase)) && match.size() > 1)
    {
        symbol = match.str(1);
        operationOrDirective = match.str(2);
        operationCode = "4C";
        hasSym = true;
        return true;
    }
    if (std::regex_search(line, match, regex("^\\s*([a-zA-Z]{1}\\w+)\\s+([\\+]?RSUB)\\s+.*$",regex_constants::icase)) && match.size() > 1)
    {

        symbol = match.str(1);
        operationOrDirective = match.str(2);
        operationCode = "4C";
        hasSym = true;
        return true;
    }
    else if (regex_search(line,match,std::regex("^\\s*([\\+]?RSUB)\\s*$",regex_constants::icase)))
    {

        operationOrDirective = match.str(1);
        operationCode = "4C";
        hasSym = false;
        comment = "";
        return true;
    }
    else if (regex_search(line,match,std::regex("^\\s*([\\+]?RSUB)\\s+.*$",regex_constants::icase)))
    {
        operationOrDirective = match.str(1);
        operationCode = "4C";
        hasSym = false;
        return true;
    }
    return false;
}

bool parser::matchInstructionWithNoSymbol(string line)
{
    std::smatch match;
    if (std::regex_search(line, match, instructionWithNoSymbol) && match.size() == 3)
    {
        string oprtn ;
        string oprnd;
        try
        {
            oprtn = match.str(1);
            oprnd = match.str(2);
            toUpper(oprtn);
        }
        catch(std::length_error e)
        {
            return false;
        }
        bool isFormatFour = false;
        if(oprtn.at(0) == '+')
        {
            isFormatFour  = true;
            oprtn.erase(oprtn.begin());
        }
        unordered_map<string,operation> instructionTable = reader.getTable();
        unordered_map<string,operation>::iterator itr = instructionTable.find(oprtn);
        if(itr != instructionTable.end()) //operation found in symbol table
        {
            if(!isValidInstructionOperand(oprnd,itr->second))
            {
                return false;
            }
            operationOrDirective = match.str(1);
            operand = oprnd;
            operation op = itr->second;
            operationCode = op.getOpCode();
            format = op.getFormat();
            if(isFormatFour)
            {
                if(format == 2 )
                {
                    //set error
                    hasErr = true;
                    err = "+ sign before format 2 instruction";
                    return false;
                }
                else
                {
                    format = 4;
                }
            }
            return true;
        }
    }
    return false;

}
bool parser::matchInstructionWithSymbol(string line)
{
    std::smatch match;
    if (std::regex_search(line, match, parser::instructionWithSymbol) && match.size() > 1)
    {
        string smbl;
        string oprtn ;
        string oprnd ;
        try
        {

            smbl = match.str(1);
            oprtn = match.str(2);
            oprnd = match.str(3);
            toUpper(oprtn);
        }
        catch(std::length_error e )
        {
            return false;
        }
        bool isFormatFour = false;
        if(oprtn.at(0) == '+')

        {
            isFormatFour = true;
            oprtn.erase(oprtn.begin());
        }
        unordered_map<string,operation> instructionTable = reader.getTable();
        unordered_map<string,operation>::iterator itr = instructionTable.find(oprtn);
        if(itr != instructionTable.end()) //operation found in symbol table
        {
            if(!isValidInstructionOperand(oprnd,itr->second))
            {
                return false;
            }
            symbol = smbl;
            operationOrDirective = match.str(2);
            operand = oprnd;
            operation op = itr->second;
            operationCode = op.getOpCode();
            format = op.getFormat();
            if(isFormatFour)
            {
                if(format == 2 )
                {
                    //set error
                    hasErr = true;
                    err = "+ sign before format 2 instruction";
                    return false;

                }
                else
                {
                    format = 4;
                }
            }
            return true;
        }
    }
    return false;
}

bool parser::isValidInstructionOperand(string operand,operation op)
{
    if(regex_match(op.getMnemonic(),regex("^(shiftl|shiftr)$",regex_constants::icase)))
    {
        if(!regex_match (operand, std::regex("^[ABTXLS],\\d+$",regex_constants::icase) ))//left or right shift
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isShift = true;
        return true;
    }
    if (regex_match (operand, std::regex("^[ABSTLX],[ABSTLX]$",regex_constants::icase) )) //register to register instructions
    {
        if(op.getRegisterOper() != 2 )
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isRegToReg = true;
        return true;
    }
    else if (regex_match (operand, std::regex("^[ABSTLX]$",regex_constants::icase) )) // 1 register operation
    {
        if(op.getRegisterOper() != 1 || op.hasMemoryOper())
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        return true;
    }

    else if (regex_match (operand, std::regex("^=C'.*'$",regex_constants::icase) )) //character literal
    {
        if(op.getRegisterOper() != 0 || !op.hasMemoryOper() )
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isLit  = true;
        return true;
    }

    else if (regex_match (operand, std::regex("^=X'[ABCDEF\\d]+'$",regex_constants::icase) )) //hexadecimal literal
    {
        if(op.getRegisterOper() != 0 || !op.hasMemoryOper() )
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        if(operand.length()%2 != 0)
        {
            hasErr = true;
            err = "hexadecimal literal with odd length";
            return false;
        }
        isLit = true;
        return true;
    }
    else if (regex_match (operand, std::regex("^=\\d+$",regex_constants::icase) )) //decimal literal
    {
        if(op.getRegisterOper() != 0 || !op.hasMemoryOper() )
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isLit = true;
        return true;
    }
    else if (regex_match (operand, std::regex("^0[ABCDEF]{1}[ABCDEF\\d]*$",regex_constants::icase) ))//hexadecimal beg with ABCDEF
    {
        if(!op.hasMemoryOper() || op.getRegisterOper() != 0)
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        return true;
    }
    else if(regex_match (operand, std::regex("^[a-zA-Z]{1}\\w*$",regex_constants::icase) ))//symbolic memory operations
    {
        if(!op.hasMemoryOper()|| op.getRegisterOper() != 0)
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isSymbolic = true;
        return true;
    }
    else if(regex_match (operand, std::regex("^[ABCDEF\\d]+$",regex_constants::icase) ))//hexadecimal
    {
        if(!op.hasMemoryOper())
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        return true;
    }

    else if (regex_match (operand, std::regex("^#[-]?\\d+$",regex_constants::icase) )) //immediate with numerical value;
    {
        if(!op.hasMemoryOper() || op.getRegisterOper() != 0)
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isImm = true;
        return true;
    }
    else if (regex_match (operand, std::regex("^\\#[a-zA-Z]{1}\\w+$",regex_constants::icase) )) //immediate with a symbol
    {
        if(!op.hasMemoryOper() || op.getRegisterOper() != 0)
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isImm = true;
        isSymbolic = true;
        return true;
    }
    else if (regex_match (operand, std::regex("^\\@[abcdef\\d]+$",regex_constants::icase) )) //indirect addressing
    {
        if(!op.hasMemoryOper() || op.getRegisterOper() != 0)
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isIndir = true;

        return true;
    }
    else if (regex_match (operand, std::regex("^\\@[a-zA-Z]{1}\\w+$",regex_constants::icase) )) //indirect addressing with symbol
    {
        if(!op.hasMemoryOper())
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isIndir = true;
        isSymbolic = true;
        return true;
    }
    else if (regex_match (operand, std::regex("^[abcdef\\d]+,\\s*X$",regex_constants::icase) )) //indexing with number
    {
        if(!op.hasMemoryOper())
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isIndex = true;
        return true;
    }
    else if (regex_match (operand, std::regex("^[a-zA-Z]{1}\\w+,\\s*X$",regex_constants::icase) )) //indexing with symbol
    {
        if(!op.hasMemoryOper())
        {
            hasErr = true;
            err = "invalid operand for the operation";
            return false;
        }
        isIndex = true;
        isSymbolic = true;
        return true;
    }
    else  if (regex_match (operand, std::regex("^(\\d+|\\w+)([\\*\\+/-](\\d+|\\w+))+$") ) && op.getRegisterOper() == 0 && op.hasMemoryOper() )
    {
        isExp = true;
        return true;
    }
    hasErr = true;
    err = "invalid instruction operand";
    return false;

}

bool parser::matchesDirectiveOrInstruction(string labelOrSymbol)
{
    toUpper(labelOrSymbol);
    unordered_map<string,operation> instMap = reader.getTable();
    unordered_map<string,operation>::iterator itr = instMap.find(labelOrSymbol);
    if(itr != instMap.end())
    {
        hasErr = true;
        err = "label or symbol matches an instruction";
        return true;
    }
    unordered_map<string,string> drctvMap = dReader.getDirTable();
    unordered_map<string,string>::iterator itr2 = drctvMap.find(labelOrSymbol);
    if(itr2 != drctvMap.end())
    {
        hasErr = true;
        err = "label or symbol matches a directive";
        return true;
    }
    return false;
}



string parser::getOperationCode()
{
    return operationCode;
}

bool parser::matchesDirective(string line)
{
    std::smatch match;
    if (matchesDirectiveWithSymbolAndOperand(line))
    {
        isDir = true;
        isVld = true;
        isComm = false ;
        hasOprnds = true;
        hasSym = true;
        return true;

    }
    else if(matchesDirectiveWithNoSymbolAndOperand(line))
    {
        isDir = true;
        isVld = true;
        isComm = false ;
        hasOprnds = true;
        hasSym = false;
        return true;
    }
    else if(matchesDirectiveWithNoSymbolNorOperand(line))
    {
        isDir = true;
        isVld = true;
        isComm = false ;
        hasOprnds = false;
        hasSym = false;
        return true;

    }
    isVld = false;
    return false;

}
bool parser::matchesDirectiveWithSymbolAndOperand(string line)
{
    std::smatch match;
    if (std::regex_search(line, match, parser::directiveWithSymbolAndOperand) && match.size() > 1)
    {
        string smbl ;
        string dctv;
        string oprnd ;
        try
        {
            smbl = match.str(1);
            dctv = match.str(2);
            oprnd = match.str(3);
            toUpper(dctv);
        }
        catch(std::length_error e )
        {
            return false;
        }
        unordered_map<string,string> drctvTable = dReader.getDirTable();
        unordered_map<string,string>::iterator itr = drctvTable.find(dctv);

        if(itr != drctvTable.end()) //found
        {
            if(isAcceptedDirectiveOperand(oprnd,dctv))
            {
                symbol = smbl;
                operationOrDirective = dctv;
                operand = oprnd;
                return true;
            }
            else
            {
                return false;
            }
        }

    }
    return false;
}
bool parser::matchesDirectiveWithNoSymbolAndOperand(string line)
{
    std::smatch match;
    if(std::regex_search(line, match, parser::directiveWithNoSymbolAndOperand) && match.size() > 1)
    {
        string dctv ;
        string oprnd ;
        try
        {
            dctv = match.str(1);
            oprnd = match.str(2);
            toUpper(dctv);
        }
        catch(std::length_error e )
        {
            return false;

        }
        unordered_map<string,string> drctvTable = dReader.getDirTable();
        unordered_map<string,string>::iterator itr = drctvTable.find(dctv);
        if(itr != drctvTable.end()) //found
        {
            if(isAcceptedDirectiveOperand(oprnd,dctv))
            {
                operationOrDirective = dctv;
                operand = oprnd;
                return true;
            }
            else
            {
                return false;
            }


        }
    }
    return false;

}
bool parser::matchesDirectiveWithNoSymbolNorOperand(string line)
{
    std::smatch match;
    if(std::regex_search(line, match, parser::directiveWithNoSymbolNorOperand) && match.size() > 1)
    {
        string dctv ;
        try
        {
            dctv = match.str(1);
            toUpper(dctv);
        }
        catch(std::length_error e )
        {
            return false;
        }
        unordered_map<string,string> drctvTable = dReader.getDirTable();
        unordered_map<string,string>::iterator itr = drctvTable.find(dctv);
        if(itr != drctvTable.end()) //found
        {
            if(!regex_match(dctv,regex("^(nobase|ltorg|org|end)$",regex_constants::icase)))
            {
                hasErr = true;
                err = "invalid instruction operand";
                return false;
            }
            operationOrDirective = dctv;
            return true;

        }

    }
    return false;

}


bool parser::isAcceptedDirectiveOperand(string oprnd,string oprtn)
{

    if (regex_match (oprnd, std::regex("^\\d+(,\\d+)*$") ) && regex_match(oprtn,regex("(word)",regex_constants::icase))) //decimal
    {

        return true;
    }
    else if (regex_match (oprnd, std::regex("^\\w+$") ) && regex_match(oprtn,regex("(end|org|equ|base|word)",regex_constants::icase))) //symbolic operand
    {
        return true;
    }
    else if (regex_match (oprnd, std::regex("^\\d+$") ) && regex_match(oprtn,regex("(resb|resw)",regex_constants::icase)))//byte
    {
        return true;
    }
    else if (regex_match (oprnd, std::regex("^X'[\\dABCDEF]+'$") ) && regex_match(oprtn,regex("(byte)",regex_constants::icase))) //hexadecimal operand
    {
        return true;
    }
    else if (regex_match (oprnd, std::regex("^C'.+'$")) && regex_match(oprtn,regex("(byte)",regex_constants::icase))) //character operand
    {
        return true;
    }
    else if (regex_match (oprnd, std::regex("^[ABCDEF\\d]+$") ) && regex_match(oprtn,regex("(start|equ)",regex_constants::icase))) //hexadecimal locations for start or end
    {
        return true;
    }
    else if (regex_match (oprnd, std::regex("^(\\d+|\\w+)([\\*\\+/-](\\d+|\\w+))+$") ) && regex_match(oprtn,regex("(org|equ|resb|resw|word)",regex_constants::icase)))
    {
        isExp = true;
        return true;
    }
    hasErr = true;
    err = "invalid directive operand";
    return false;

}

void parser::toUpper(string &instructionOrDirective)
{
    for(unsigned counter =  0 ; counter < instructionOrDirective.length() ; counter ++)
    {
        char a = instructionOrDirective.at(counter);
        if(a >= 'a' && a <= 'z')
        {
            a = a + 'A' - 'a';
            string dummy;
            dummy.append(1,a);
            instructionOrDirective.replace(counter,1,dummy);
        }
    }
}


bool parser::isComment()
{
    return isComm;
};
bool parser::hasSymbol()
{
    return hasSym;
};
bool parser::hasOperands()
{
    return hasOprnds;
}
bool parser::isDirective()
{
    return isDir;
};
bool parser::isValid()
{
    return isVld;
};

bool parser::hasWarning()
{
    return hasWrnng;
}

bool parser::hasError()
{
    return hasErr;
}

string parser::getSymbol()
{
    toUpper(symbol);
    return symbol;
}

string parser::getInsOrDir()
{
    return operationOrDirective;
}

string parser::getOperand()
{
    toUpper(operand);
    return operand;
}

string parser::getWarning()
{
    return warning;
}

string parser::getError()
{
    return err;
}

int parser::getFormat()
{
    return format;
}
void parser::handleLine(string line)
{
    initialize();
    if( !matchComment(line))
    {
        if(!matchesOperation(line))
        {
            if(!matchesDirective(line))
            {
                if(!hasErr)
                {
                    hasErr = true;
                    err = "neither matched a comment, an instruction nor a directive";
                }
                isVld = false;
            }
        }
    }
}
bool parser::isBlank(string line)
{
    for(unsigned counter = 0 ; counter < line.length() ; counter ++)
    {
        if(line.at(counter) == ' ' || line.at(counter) == '\t' || line.at(counter) == '\n')
        {
            continue;
        }
        else
        {
            return false;
        }
    }
    return true;
}

//string   parser::getComment(string line, int numberOfPrecedingGroups)
//{
//    string comm;
//    regex r;
//    smatch match;
//    if(numberOfPrecedingGroups == 1 )
//    {
//        r.assign("^\\s*\\S+\\s+(.+)$");
//        if(regex_search(line,match,r) )
//        {
//            comm = match.str(1);
//        }
//        else
//        {
//            comm = "";
//        }
//    }
//    else if(numberOfPrecedingGroups == 2)
//    {
//        r.assign("^\\s*(\\S+)\\s+(\\S+)\\s+(.+)$");
//
//        if(regex_search(line,match,r)  )
//        {
//            cout << "here we are" << endl;
//            comm = match.str(3);
//        }
//        else
//        {
//            comm = "";
//        }
//
//    }
//    else if (numberOfPrecedingGroups == 3 )
//    {
//        r.assign("^\\s*\\S+\\s+\\S+\\s+\\S+\\s+(.+)$");
//        if(regex_search(line,match,r) )
//        {
//            comm = match.str(1);
//        }
//        else
//        {
//            comm = "";
//        }
//
//    }
//    return comm;
//
//}

string parser::getComment()
{
    return comment;
}


bool parser::isImmediate()
{
    return isImm;
}

bool parser::isIndirect()
{
    return isIndir;
}

bool parser::isIndexing()
{
    return isIndex;
}
bool parser::isLiteral()
{
    return isLit;
}


void parser::initialize()
{
    symbol.clear();
    operationOrDirective.clear();
    operand.clear();
    operationCode.clear();
    warning.clear();
    err.clear();
    comment.clear();
    literal.clear();
    isComm = false;
    isDir= false;
    hasSym= false;
    hasOprnds= false;
    isVld= false;
    hasWrnng= false;
    hasErr= false;
    isImm= false;
    isIndir= false;
    isIndex= false;
    isLit = false;
    isSymbolic = false;
    isRegToReg = false;
    isShift = false;
    isLitLine = false;
    isExp = false;
    format= 0;
}


void parser::handlePass2Line(string line)
{
    initialize();
    isErrorLine = false;
    address.clear();
    smatch sm;
    //14  000023                   shiftl         A,3
    if(line.at(0) == '*')
    {
        isErrorLine  = true ;
    }
    else if(matchComment(line.substr(4,line.length()-4)))
    {
        handleLine(line.substr(4,line.length()-4));
    }

    else if(regex_search(line,sm,regex("^(\\d+)\\s+([abcdef\\d]+)\\s+(=X'.*'|=C'.*'|=[-]?\\d+)\\s*$",regex_constants::icase)))
    {
        isLitLine = true;
        literal = sm.str(3);
        address = sm.str(2);
        isErrorLine  = false;
    }
    else if(regex_search(line,sm,regex("^(\\d+)\\s+([abcdef\\d]+)\\s+(.+)$",regex_constants::icase)))
    {
        string num = sm.str(1);
        stringstream st;
        st << num;
        st >> lineNumber;
        address = sm.str(2);
        handleLine(sm.str(3));
    }
    else
    {
        isErrorLine = true;
    }

}

bool parser::isErrLine()
{

    return isErrorLine;
}
bool parser::isRegisterToRegister()
{
    return isRegToReg;
}
bool parser::isShiftIns()
{
    return isShift;
}
bool parser::hasSymbolicOperand()
{
    return isSymbolic;
}

int parser::getLineNumber()
{
    return lineNumber;
}
string parser::getAddress()
{
    return address;
}
string parser::getLiteral()
{

    return literal;
}

bool parser::isLiteralLine()
{
    return isLitLine;
}

bool parser::isExpression()
{
    return isExp;
}
