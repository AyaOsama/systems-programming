#include "assemblerHeaders.h"

unordered_map<string,string> directiveReader::getDirTable()
{
    return directiveTable;
}


void directiveReader::readDirective()
{
    string line;
    ifstream myfile ("directive.txt");
    if(myfile.is_open())
    {
        while(! myfile.eof() )
        {
            getline (myfile,line); // read line from file
            string directive, operand;
            stringstream(line) >> directive >> operand ;
            directiveTable.insert(make_pair(directive,operand));
        }
        myfile.close();
    }
    else cout << "Unable to open file";


}
