#include "assemblerHeaders.h"

operation::operation(string mnem,string opCd,int frmt, bool hasOp , bool memory , int regit)  // constructor
{
    mnemonic = mnem;
    opCode = opCd;
    format = frmt;
    hasOperand = hasOp;
    memoryOper = memory;
    registerOper = regit;
}

int operation::getFormat()
{
    return format;
}
bool operation::hasOperands()
{
    return hasOperand ;   // returns true if the operation requires operand
}
string operation::getOpCode()
{
    return opCode;
}
string operation::getMnemonic()
{
    return mnemonic ;
}

bool operation::hasMemoryOper()
{
    return memoryOper;
}

int operation::getRegisterOper()
{
    return registerOper;
}
